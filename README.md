Client should be able to talk to server
Server and client using simple authentication (handshake)
Server and client exchanging messages


Part 1 Create little ecosystem
Create server which will listen on local TCP port
Create client which will connect to server on local TCP port
Create common library for sharing object
Part 2 Create handshake exchange
Client should send handshake exchange message
Server should be able to read handshake message
Server should response to client on sucessfull handshake
Part 3 Create user balance update
Client should send request for balance with amount of money
Server should receive balance update message and read it
Server should should perform balance update operation and inform client about it